#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2019 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Crop a video to 21:9
#
# deps: ffmpeg, coreutils
###

mkdir -p cropped

for i in "$@"; do
	[ -e "$i" ] || continue
	# ffmpeg -i "$i" -vf "crop=ih*21/9:ih" -c:a copy -c:s copy "cropped/${i##*/}"
	ffmpeg -i "$i" -vf "crop=1920:820" -c:a copy -c:s copy "cropped/${i##*/}"
done
