#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.


"""Take a SMB link from Outlook Web App and download a file"""
# deps: passtodict, pysmbc


import sys
import re
from base64 import b64decode
from urllib.parse import unquote
import smbc
import passtodict
import subprocess

WINDRIVES = [
    (r'^G:/', 'smb://Mikro3vm/Data01/'),
    (r'^H:/', 'smb://Mikro3vm/Data02/'),
    (r'^P:/', 'smb://Mikro3vm/Public/'),
    (r'^V:/', 'smb://Mikro3vm/Vymena/'),
]


def auth_fn(server, share, workgroup, username, password):
    """auth_fn for smbs.Context,
    should return a tuple of strings: workgroup, username, and password."""
    mel = passtodict('work/mikro.mikroelektronika.cz')
    return mel['workgroup'], mel['login'], mel.PWD


def get_uri(url):
    """Get smb:/ uri"""
    if url.startswith('file:'):
        return unquote(url).replace('file:///', 'smb:')

    if url.startswith('smb:'):
        return url

    if url.startswith('http'):
        return uri_from_email(url)

    url = url.replace('\\', '/')
    for drive_regex, smb_share in WINDRIVES:
        match = re.match(drive_regex, url, re.IGNORECASE)
        if match:
            return url.replace(match.group(), smb_share)


def uri_from_email(url):
    """from Outlock Web App url"""
    base64 = re.search('(?<=REF=.{52}).*',  # ignore first 39 bytes
                       url).group().replace('.', '=')
    decoded = b64decode(base64, altchars=b'-_').decode()
    unquoted = unquote(decoded).replace('\\', '/')
    return re.sub(r'^.*?file:/*', r'smb://', unquoted)


def download(uri):
    print("Downloading ‘{}’".format(uri))

    location, filename = uri.rsplit('/', maxsplit=1)
    ctx = smbc.Context(auth_fn=auth_fn)
    remotefile = ctx.open(uri)

    path = "/tmp/" + filename
    with open(path, 'wb') as localfile:
        localfile.write(remotefile.read())

    return path


def main(url):
    uri = get_uri(url)
    path = download(uri)
    status = subprocess.check_call(['rifle', path])     # or xdg-open
    sys.exit(status)


if __name__ == '__main__':
    main(*sys.argv[1:])

    # main('https://mikro.mikroelektronika.cz/owa/redir.aspx?REF='
    #      'gJkHBK6NFeVhH8uy4664wjlhKmx6zY6sHTGp6bFYxlN7sXacGw_VCAFmaWxlOi8v'
    #      'L1xcTWlrcm8zdm1cVnltZW5hXEZyaWVkbFxfT2JqZWRuw6F2a3lcRG9icm90eVxP'
    #      'YmplZG7DoXZrYV_FmcOtamVuXzIwMTcueGxzeA..')

    # url = V_URI
    # uri = get_uri(url)
    # print(uri)
