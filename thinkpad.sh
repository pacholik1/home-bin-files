#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Do something on thinkpad
#
# debs: ssh, coreutils
###

file="$(readlink -f "$@")"
url="http://rpi${file#/media}"

case $(basename "$0") in
	thinkpad.play.sh)
		command='mpv --really-quiet "'"$url"'"'
		;;
	thinkpad.copyurl.sh)
		# command='(printf '"'%s'"' "'"$url"'" | xclip -selection clipboard)'
		command='(printf '"'%s'"' "'"$url"'" | wl-copy)'
		;;
	# thinkpad.wget.sh)
	# 	target="/tmp/$(basename "$file")"
	# 	command='wget --background --quiet --directory-prefix=/tmp "'"$url"'"'
	# 	;;
	*)
		command='rifle "'"$url"'"'
		;;
esac

exec ssh pacholik@thinkpad.home '
export DISPLAY=:0
'"$command"' </dev/null >/dev/null 2>/dev/null &
'
