#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Open mounted remote directory
#
# deps: gvfs-bin, ranger, sed, findutils
###

# $1: smb://DMIKRO;pachol@mikro3vm/vymena/
#path=$XDG_RUNTIME_DIR/gvfs/`gvfs-info "$1" | sed -n 's/^\s*id::filesystem: //p'`
#$TERMCMD -e ranger $path
gvfs-info "$1" | sed -n "s|^\\s*id::filesystem: |$XDG_RUNTIME_DIR/gvfs/|p" | xargs rifle
