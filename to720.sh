#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Convert a video to 720 AVC, audio and subtitles being preserved.
#
# deps: ffmpeg, coreutils
###

mkdir -p 720

for i in "$@"; do
	[ -e "$i" ] || continue
	target="720/${i##*/}"
	ffmpeg -i "$i" -s hd720 -c:a copy -c:s copy "$target" || rm "$target"
done
