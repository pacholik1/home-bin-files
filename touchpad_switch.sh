#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Toggle touchpad
#
# deps: xinput, sed, libnotify-bin
###

id=$(xinput list | sed -En '/[Tt]ouch[Pp]ad/s/^.*id=([0-9]+).*$/\1/p')
enabled=$(xinput list-props "$id" | sed -En '/Device Enabled/s/^.*([0-9]+)$/\1/p')

if [ "$enabled" = "0" ]; then
	xinput set-prop "$id" "Device Enabled" 1
	notify-send -i input-touchpad-symbolic "Touchpad is enabled."
else
	xinput set-prop "$id" "Device Enabled" 0
	notify-send -i input-touchpad-symbolic "Touchpad is disabled."
fi
