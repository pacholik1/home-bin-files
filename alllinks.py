#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

"""Find all links on a webpage"""
# deps: beautifulsoup4, httplib2

import bs4
import httplib2
import urllib


def main(url):
    h = httplib2.Http()
    response, content = h.request(url)
    soup = bs4.BeautifulSoup(content, 'lxml')

    for tag in soup.find_all('a'):
        tag = tag   # type: bs4.element.Tag
        try:
            relative = tag.attrs['href']
        except KeyError:
            continue
        absolute = urllib.parse.urljoin(url, relative)
        print(absolute)


if __name__ == '__main__':
    import sys
    sys.exit(main(*sys.argv[1:]))
