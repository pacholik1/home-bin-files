#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

"""Convert md to html"""
# deps: Markdown

import os
import sys
from markdown import markdown
import webbrowser


def main(mdfilename, htmlfilename=None):
    if not htmlfilename:
        title, ext = os.path.splitext(mdfilename)
        # htmlfilename = re.sub(r".md$", r".html", mdfilename)
        htmlfilename = title + '.html'

    with open(mdfilename, mode='r', encoding="UTF-8") as f:
        md = f.read()
    # body = markdown(escape(md))
    body = markdown(md)

    html = f"""<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{title}</title>
    </head>
    <body>
        {body}
    </body>
</html>"""

    with open(htmlfilename, mode='w', encoding="UTF-8") as f:
        f.write(html)

    webbrowser.open(htmlfilename)


if __name__ == '__main__':
    main(*sys.argv[1:])
