#!/bin/sh -e

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2020 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Check if the package is newer than already installed
###

package="$1"
pkgver="$2"

instver=$(apt-cache policy "$package" | sed -n '/  Installed: /s///p')
printf 'package: %s\ninstalled: %s\n' "$pkgver" "$instver"

[ "$instver" = '(none)' ] && exit 0
highver=$(printf '%s\n' "$instver" "$pkgver" | sort -rV | sed '1q')

[ "$instver" != "$highver" ]
