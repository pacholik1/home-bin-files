#!/bin/sh -e

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Download a package from PyPI, create deb package, install it and upload to mega.nz
#
# deps: python-stdeb, dpkg, megatools, coreutils
###

echo() { printf '\n\e[4m%s\e[0m\n' "$*"; }
megals /Root > /dev/null 2>&1 &
export DEB_BUILD_OPTIONS=nocheck

python=python3
case "$1" in
	-2) python=python2; shift ;;
	-3) python=python3; shift ;;
esac

cd /tmp
echo REMOVING "$1"* and deb_dist
rm "$1"* || true
[ -e deb_dist ] && rm -r deb_dist

echo DOWNLOADING
archive=$($python /usr/bin/pypi-download "$1" | sed '/OK: /s///')
echo CREATING DEB
$python /usr/bin/py2dsc-deb "$archive"
echo INSTALLING
sudo dpkg -i deb_dist/*.deb

echo WAITING FOR megals
wait
echo UPLOADING TO MEGA
megaput deb_dist/*.deb --path=/Root/debs
