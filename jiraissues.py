#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

"""Lists all my jira issues"""
# deps: passtodict, jira

import os
import sys
import warnings
import passtodict
from jira import JIRA


_commands = {}


def command(f):
    _commands[f.__name__] = f


def get_cred():
    cred = passtodict('work/mikro.mikroelektronika.cz')
    username = cred.LOGIN
    password = cred.PWD
    return username, password


@command
def printall():
    _stderr = sys.stderr
    with open(os.devnull, 'w') as null:
        sys.stderr = null
    jira = JIRA(server='https://jira.mikroelektronika.cz',
                basic_auth=get_cred(),
                options={'verify': False})
    sys.stderr = _stderr

    issues = jira.search_issues(
        'project=ASW AND '
        'sprint IN openSprints() AND '
        'assignee=currentUser() '
        'ORDER BY key',
        maxResults=1000)

    for i in issues:
        print(f'{i.key} – {i.fields.summary}')


@command
def help():
    print("commands:")
    for command in _commands:
        print(f'  {command}')


def main(command):
    _commands.get(command, help)()


if __name__ == '__main__':
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        main(*sys.argv[1:])
