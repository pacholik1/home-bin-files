#!/bin/sh -e

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Today’s NASA Astronomy Picture of the Day as a wallpaper.
#
# append somthing like this to your crontab:
#     0 * * * * ~/bin/apod.sh 2> /tmp/apod.log
#
# create file /etc/wicd/scripts/postconnect/apod.sh with this command:
#     su -l username -c .../apod.sh
#
# deps: wget, feh, jq, libnotify-bin, coreutils
###

### set env vars so it works with cron
export DISPLAY=:0 
DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus"
export DBUS_SESSION_BUS_ADDRESS

picdir="$HOME/pics/apod"
symlink="$HOME/pics/apod.jpg"
json="/tmp/apod.json"

### exit if we already have today’s json
if [ "$(jq --raw-output '.date' "$json")" = "$(date +%F)" ]; then
	jq --raw-output '.explanation' "$json"
	feh --no-fehbg --bg-fill "$symlink"
	false
fi

### dowlnload json
wget -qO "$json" 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'
url=$(jq --raw-output --exit-status '.hdurl' "$json")
title=$(jq --raw-output '.title' "$json")
desc=$(jq --raw-output '.explanation' "$json" | fmt --uniform-spacing)

### dowlnload picture
mkdir -p "$picdir"
# oldpic="$(readlink "$symlink")"
newpic="$picdir/$(basename "$url")"
wget -cqO "$newpic" "$url"
ln -sf "$newpic" "$symlink"
# rm "$oldpic"

### set background
feh --no-fehbg --bg-fill "$symlink"
echo "$title"
echo "$desc"
notify-send --expire-time 20000 "$title" "$desc"
