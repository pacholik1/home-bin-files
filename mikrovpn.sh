#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2020 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

passcred work/mikro.mikroelektronika.cz | (
	read -r user
	sudo openconnect \
		--user="$user" \
		--passwd-on-stdin \
		--certificate="$HOME/work/vpn/mycert.pem" \
		--sslkey="$HOME/work/vpn/mykey.pem" \
		--servercert='sha256:fafc7521162e89231eea0c1eb54dfcb87c1a1b26a5119c0e68ad5e79f39126de' \
		'fw.mikroelektronika.cz'
)
