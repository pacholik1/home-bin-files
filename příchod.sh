#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Tell me what time I got into the office
#
# deps: passcred, wget, sed
###

passcred work/mikro.mikroelektronika.cz | (
	read -r user
	read -r pass
	url="http://$user:$pass@intranet/SitePages/Home.aspx"
	wget -qO- "$url" | sed -En '/Př&#237;chod/s|.*<td>([0-9 .:]+)</td>.*|\1|p'
)
