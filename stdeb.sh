#!/bin/sh -e

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Create deb package from python project, install it and upload to PyPI and mega.nz
#
# deps: python-stdeb, dpkg, passcred, twine, megatools, coreutils
###

echo() { printf '\n\e[4m%s\e[0m\n' "$*"; }
megals /Root > /dev/null 2>&1 &
export DEB_BUILD_OPTIONS=nocheck

python=python3
while :; do
	case "$1" in
		-2) python=python2; shift ;;
		-3) python=python3; shift ;;
		--pypi) pypi=1; shift ;;
		*) break
	esac
done

[ -n "$1" ] && cd "$1"

echo REMOVING dist AND deb_dist
[ -e dist ] && rm -r dist
[ -e deb_dist ] && rm -r deb_dist

echo CREATING PACKAGES
$python setup.py --command-packages=stdeb.command bdist_deb sdist bdist_wheel
echo INSTALLING
sudo dpkg -i deb_dist/*.deb

if [ "$pypi" ]; then
	echo UPLOADING TO PYPI
	passcred dev/pypi.org | (
		read -r user
		read -r pass
		twine upload -s -u"$user" -p"$pass" dist/*
	)
fi

echo WAITING FOR megals
wait
echo UPLOADING TO MEGA
megaput deb_dist/*.deb --path=/Root/debs
