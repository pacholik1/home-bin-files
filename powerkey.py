#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

# deps: passtodict, requests-ntlm

import sys
import passtodict
import requests
from requests_ntlm import HttpNtlmAuth
import bs4


def download():
    cred = passtodict('work/mikro.mikroelektronika.cz')
    url = "http://intranet/SitePages/Home.aspx"
    response = requests.get(
        url,
        auth=HttpNtlmAuth(f"{cred['workgroup']}\\{cred.LOGIN}", cred.PWD))
    return response.text


def find_table(text: str):
    soup = bs4.BeautifulSoup(text, 'lxml')
    td = soup.find('td', text='Osobní číslo')
    return td.parent.parent


def gen_dict(table: bs4.element.Tag):
    dct = {}
    for tr in table.find_all('tr', recursive=False):
        tdk, tdv = tr.find_all('td', recursive=False, limit=2)
        dct[tdk.text] = tdv.text
    return dct


def main():
    text = download()
    table = find_table(text)
    dct = gen_dict(table)
    from pprint import pprint
    pprint(dct)


if __name__ == '__main__':
    main(*sys.argv[1:])
