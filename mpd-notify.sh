#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Notify when the song changes
#
# deps: mpd, netcat, libnotify-bin, coreutils
###

#you can grab covers with pycoverart http://code.google.com/p/pycoverart/
cover_folder="$HOME/.music/covers/"

#edit by yourself
pass=""
nc() {
	netcat localhost 6600
}

#get unique id for currently playing song
getsongid() {
	printf 'status\nclose\n%s' "$pass" | nc | sed -ne "/^state: stop$/ q" -e "/^songid: / s///p"
}
# mpd-wait https://bbs.archlinux.org/viewtopic.php?pid=606304#p606304
change() {
#	while [ "$songid" = "`getsongid`" ]; do
		printf 'idle\n%s' "$pass" | cat - "$pipe" | nc | sed -n "/^changed.*/ s//close/w $pipe"		
#	done
	export songid
	songid=$(getsongid)
}

updateinfo() {
	IFS="
"
	current=$(printf 'currentsong\nclose\n%s' "$pass" | nc)
	artist=$(for i in $current; do echo "$i"; done | sed -n "/^Artist: /s///p")
	album=$(for i in $current; do echo "$i"; done | sed -n "/^Album: /s///p")
	title=$(for i in $current; do echo "$i"; done | sed -n "/^Title: /s///p")

	[ "$(ls "$cover_folder"/*"$artist"-"$album"*)" ] && \
		cover="$(find "$cover_folder" -iname "$artist-$album.*" 2> /dev/null | head -n 1)" || \
		cover="sonata"

	return 0
}

#run with -d to notify whenever track changes
if [ "$1" = "-d" ]; then
	pipe=$(mktemp)
	rm "$pipe"
	mkfifo "$pipe"
	
	updateinfo
	while change; do
		updateinfo
		notify-send -i "$cover" "$artist" "$title"
		printf "bfaed"
	done
fi

#otherwise notify anyway
updateinfo
notify-send -i "$cover" "$artist" "$title"
exit 0
