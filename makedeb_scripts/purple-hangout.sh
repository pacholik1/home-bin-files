#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -n '/Release: /s///p' purple-hangouts.spec)"
isnewer.sh 'purple-hangouts' "$version"
dir="purple-hangout_$version"

set -- libpurple-dev libjson-glib-dev libglib2.0-dev libprotobuf-c-dev protobuf-c-compiler
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: purple-hangouts
Version: %s
Section: net
Priority: optional
Architecture: amd64
Depends: libpurple0
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: Hangouts Plugin for libpurple
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/lib/purple-2"
mkdir -p "$dir/usr/share/doc/purple-hangouts"
mkdir -p "$dir/usr/share/pixmaps/pidgin/protocols/16"
mkdir -p "$dir/usr/share/pixmaps/pidgin/protocols/22"
mkdir -p "$dir/usr/share/pixmaps/pidgin/protocols/48"
cp libhangouts.so "$dir/usr/lib/purple-2"
cp README.md "$dir/usr/share/doc/purple-hangouts"
cp hangouts16.png "$dir/usr/share/pixmaps/pidgin/protocols/16/hangouts.png"
cp hangouts22.png "$dir/usr/share/pixmaps/pidgin/protocols/22/hangouts.png"
cp hangouts48.png "$dir/usr/share/pixmaps/pidgin/protocols/48/hangouts.png"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
