#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -En "/^  *version:/s/[^']*'([^']*)'.*/\1/p" meson.build)"
isnewer.sh 'waybar' "$version"
dir="waybar_$version"

set -- meson clang-tidy gobject-introspection libdbusmenu-gtk3-dev libfmt-dev libgirepository1.0-dev libgtk-3-dev libgtkmm-3.0-dev libinput-dev libjsoncpp-dev libmpdclient-dev libnl-3-dev libnl-genl-3-dev libpulse-dev libsigc++-2.0-dev libspdlog-dev libwayland-dev scdoc
sudo aptitude -y install "$@"
meson build
ninja -C build

mkdir -p "$dir/DEBIAN"

printf 'Package: waybar
Version: %s
Section: x11
Priority: optional
Architecture: amd64
Depends: libgtkmm-3.0-1v5, libjsoncpp1, libinput10, libsigc++-2.0-0v5, libwayland-bin, libspdlog1
Recommends: libgtk-3-0, gobject-introspection, libpulse0, libnl-3-200, sway, libdbusmenu-gtk3-4, libmpdclient2
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: Highly customizable Wayland bar for Sway and Wlroots based compositors.
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
mkdir -p "$dir/lib/systemd/system"
cp build/waybar "$dir/usr/bin/waybar"
cp build/waybar.service "$dir/lib/systemd/system"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
