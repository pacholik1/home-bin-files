#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

sudo make install-deb
sudo aptitude markauto debhelper devscripts equivs gdebi-core

wait
megaput ../*.deb --path=/Root/debs
rm ../*.deb
