#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -n '/PACKAGE_VERSION = /s///p' Makefile)"
isnewer.sh 'redshift-wlr' "$version"
dir="redshift-wlr_$version"

set -- autopoint intltool libdrm-dev libxcb1-dev libxcb-randr0-dev libx11-dev libxxf86vm-dev libwayland-dev libglib2.0-dev
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: redshift-wlr
Version: %s
Section: x11
Priority: optional
Architecture: amd64
Depends: libc6, libdrm2, libglib2.0-0, libx11-6, libxcb-randr0, libxcb1, libxxf86vm1
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: Adjusts the color temperature of your screen
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/local/bin"
mkdir -p "$dir/usr/lib/systemd/user"
mkdir -p "$dir/usr/share/applications"
cp src/redshift "$dir/usr/local/bin/redshift-wlr"
sed 's /usr/local/bin/redshift /usr/local/bin/redshift-wlr ' \
	data/systemd/redshift.service > "$dir/usr/lib/systemd/user/redshift-wlr.service"
sed 's Exec=redshift Exec=redshift-wlr ' \
	data/applications/redshift.desktop > "$dir/usr/share/applications/redshift-wlr.desktop"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
