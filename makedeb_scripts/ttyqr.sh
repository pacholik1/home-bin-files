#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version=
eval version="$(sed -n '/^#define VERSION /s///p' ttyqr.c)"	# eval for quotes
isnewer.sh 'ttyqr' "$version"
dir="ttyqr_$version"

set -- libqrencode-dev
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: ttyqr
Version: %s
Section: graphics
Priority: optional
Architecture: amd64
Depends: libqrencode3
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: ttyQR - draw QR codes straight into the terminal
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
cp ttyqr "$dir/usr/bin/ttyqr"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
