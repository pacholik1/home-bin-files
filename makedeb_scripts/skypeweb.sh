#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

set -- libpurple-dev libjson-glib-dev cmake gcc
sudo aptitude -y install "$@"
cd skypeweb/build/
mkdir -p build
cmake ..
cpack

sudo aptitude -y markauto "$@"
wait
megaput ./*.deb --path=/Root/debs
