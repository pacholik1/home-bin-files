#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -n '/VERSION = /s///p' Makefile)"
isnewer.sh 'lemonbar-xft' "$version"
dir="lemonbar-xft_$version"

set -- libx11-xcb-dev libxcb-randr0-dev libxcb-xinerama0-dev libxcb-ewmh-dev libxcb-render-util0-dev libxft-dev
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: lemonbar-xft
Version: %s
Section: x11
Priority: optional
Architecture: amd64
Depends: libc6, libxcb-randr0, libxcb-xinerama0, libxcb-ewmh2, libxcb1, libxft2
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: lightweight bar based on XCB
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
cp lemonbar "$dir/usr/bin/lemonbar"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
