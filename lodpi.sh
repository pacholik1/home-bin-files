#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2019 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Set scale factors and dpi
#
# deps: x11-xserver-utils, coreutils
###

export GDK_SCALE=1
# export QT_DEVICE_PIXEL_RATIO=1
# export QT_AUTO_SCREEN_SCALE_FACTOR=1

xrandr --output eDP-1 --scale 1x1 --dpi 96
echo 'URxvt.font: xft:DejaVu Sans Mono for Powerline:size=10' | xrdb
