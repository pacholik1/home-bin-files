#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# https://www.czso.cz/csu/czso/pocet-obyvatel-v-obcich-see2a5tx8j
#
# deps: openpyxl
###

import sys
import openpyxl


class Municipality:
    def __init__(self, row):
        self.lau1 = row[0].value
        self.lau2 = row[1].value
        self.name = row[2].value
        if not self.name:
            raise ValueError("Name can't be empty")
        self.population = int(row[3].value)
        self.male_population = int(row[4].value)
        self.female_population = int(row[5].value)
        self.average_age = float(row[6].value)
        self.male_average_age = float(row[7].value)
        self.female_average_age = float(row[8].value)

    def __str__(self):
        return f"{self.name}: {self.population}, {self.average_age}"

    def __gt__(self, other):
        return self.population > other.population


def main(xlsx):
    municipalities = []
    wb = openpyxl.load_workbook(xlsx)
    sheet = wb.get_active_sheet()

    for row in sheet.iter_rows():
        try:
            municipality = Municipality(row)
        except (ValueError, KeyError):
            pass
        else:
            municipalities.append(municipality)

    municipalities.sort(reverse=True)
    for m in municipalities:
        print(m.name)


if __name__ == '__main__':
    # '/tmp/1300721803.xlsx'
    main(*sys.argv[1:])
