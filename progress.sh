#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2017 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

tput civis
cleanup() {
	printf '\r\033[K'	# clear line
	tput cnorm
	exit "$1"
}
trap 'cleanup 1' INT TERM

c=0
p=0
while :; do
	[ $c -ge 400 ] && break
	printf ' '
	# printf '%s' "$chars" | while read -r char; do
	for char in ▏ ▎ ▍ ▌ ▋ ▊ ▉ █; do
		if [ $c -eq $p ]; then
			read -r _p
			p=${_p:-100}
			p=$(echo "($p*4+0.5)/1" | bc)
		fi

		printf '\b%s' "$char"
		sleep 0.0001
		c=$((c+1))
	done
done

cleanup 0
