#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2017 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Convert WIN1250 coded pdb to UTF8 coded txt
#
# deps: txt2pdbdoc, libc-bin
###

pdb=$1
txt=${2-${1%.*}.txt}

txt2pdbdoc -dD "$pdb" /dev/stdout | iconv -f WINDOWS-1250 -t UTF-8 > "$txt"
