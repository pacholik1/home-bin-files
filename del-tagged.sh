#!/bin/sh -e

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2020 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Delete all files tagged ‘d’ in ranger file manager.
###

# delete files
ranger --list-tagged-files=d | tr '\n' '\0' | xargs -0 rm -rf
# delete tags
sed -i '/^d:/d' ~/.config/ranger/tagged
