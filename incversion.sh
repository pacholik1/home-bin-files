#!/bin/bash

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2019 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Increase version – on a line specified by the first argument,
#                    increase the last number
# examples:
#   incversion.sh /VERSION/ myfile.sh    # pattern
#   incversion.sh 5 myfile.sh            # line number

linespec="$1"
filename="$2"
# shellcheck disable=SC2016
sed -ri "$linespec"'s/(.*)([0-9]+)(.*)/echo "\1$((\2+1))\3"/e' "$filename"
sed -rn "$linespec"p "$filename"
