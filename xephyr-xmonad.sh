#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Run xmonad in new windowed X
#
# deps: xserver-xephyr, xmonad, procps, coreutils
###

Xephyr -ac -br -noreset -screen 1024x768 :1.0 &
pid=$!
sleep 1

# AWESOME_SOURCE_DIR=~/vcs/awesome
# AWESOME_BUILD_DIR="$AWESOME_SOURCE_DIR/build"
# export LUA_PATH
# LUA_PATH="$(lua -e 'print(package.path)');$AWESOME_BUILD_DIR/lib/?.lua;$AWESOME_BUILD_DIR/lib/?/init.lua"
# DISPLAY=:1.0 $AWESOME_BUILD_DIR/awesome "$@"

DISPLAY=:1.0 xmonad "$@"

kill $pid
