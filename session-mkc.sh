#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Create kindle collection from in qutebrowser session
#
# deps: python3-mkc, grep, vipe
###

site="$1"
session="/tmp/$site"

case "$site" in
	root) regex='https://www\.root\.cz.*' ;;
	kosmonautix) regex='https?://(www\.)?kosmonautix\.cz.*' ;;
esac

grep -Po "$regex" "$session" | vipe | mkc "$site"_"$(date +%F)"
