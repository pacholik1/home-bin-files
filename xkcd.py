#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

"""Create kindle collection of xkcd strips"""
# deps: mkc, beautifulsoup4

import sys
import httplib2
from bs4 import BeautifulSoup

import mkc


def get_article(url, imgs=False, maths=True):
    context_element = ["div", {"id": "comic"}]

    h = httplib2.Http(mkc.CACHE_DIR, disable_ssl_certificate_validation=True)
    try:
        response, content = h.request(url)
    except Exception:
        print(url)
        raise
    # soup = BeautifulSoup(content)
    soup = BeautifulSoup(content, 'lxml')
    title = soup.find("title").contents[0]
    snip = soup.find(*context_element)

    img = snip.find("img")
    img["src"] = mkc.images[url, img.get("src", "")]

    content = """{snip}<p style="page-break-before:always">{alt}</p>""".format(
        snip=str(snip), alt=img["title"])
    return mkc.Article(title, content)


def main(*argv):
    r = range(1900, 2001)
    articles = [
        get_article("https://xkcd.com/{}".format(i), imgs=True, maths=False)
        for i in r
        if i not in [1953]
    ]

    page = mkc.make_multiple_page(articles,
                                  name=f"xkcd_{r.start}-{r.stop - 1}",
                                  titles=True)
    print(mkc.html_to_mobi(page))


if __name__ == '__main__':
    main(sys.argv[1:])
