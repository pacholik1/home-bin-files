#!/bin/sh

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

###
# Send file or dir to mega.nz
#
# deps: megatools
###

if [ -d "$1" ]; then
	# directory
	megamkdir "/Root/$1"
	exec megacopy -l "$1" -r "/Root/$1"
fi

case "${1##*.}" in
	deb) path="/Root/debs" ;;
	kdbx) path="/Root/keepass" ;;
	*) path="/Root" ;;
esac

# echo "$path"
exec megaput --path="$path" "$1"
