#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2016 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

"""Send a file using Gmail"""
# deps: passtodict

import os
import sys
import smtplib
import passtodict
from email.mime import text, base, multipart
from email import encoders
from base64 import b64encode


sharon = passtodict.get('dev/sharon')
login = sharon['login']
pwd = sharon.PWD


def ascii_name(name):
    """Create a name to be used in header."""
    try:
        name.encode("ascii")
    except UnicodeEncodeError:
        return "=?UTF-8?B?%s==?=" % b64encode(name.encode()).decode()
    else:
        return name


def make_attachment(payload, name="Unknown"):
    """Generate MIMEBase structure to attach to the email message.

    :param payload: str or bytes
    :param name:    Attachment label in message
    :returns:       MIMEBase
    """
    mb = base.MIMEBase('application', 'octet-stream')
    # mb.set_payload(open(attachment, "rb").read())
    mb.set_payload(payload)
    encoders.encode_base64(mb)
    mb.add_header("Content-Disposition",
                  # """attachment; filename="%s"; filename*=utf-8''%s""" % (
                  #     name.encode("ascii", "backslashreplace").decode(),
                  #     quote(name)))
                  '''attachment; filename="%s"''' % ascii_name(name))

    return mb
    # msg.attach(mb)


def send(recipient, ccs=None, subject="", body="", attachments=None):
    """Send an email using Gmail account.

    Support on IMAM and POP:
    https://support.google.com/mail/troubleshooter/1668960#ts=1665018,1665144
    """
    if ccs is None:
        ccs = []
    if attachments is None:
        attachments = []

    msg = multipart.MIMEMultipart()
    msg["To"] = recipient
    msg["Cc"] = ", ".join(ccs)
    msg["Subject"] = subject
    msg["From"] = login
    msg.attach(text.MIMEText(body.encode("utf-8"), _charset="utf-8"))

    for attachment in attachments:
        msg.attach(attachment)

    smtp = smtplib.SMTP("smtp.gmail.com", 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(login, pwd)
    smtp.sendmail(login, recipient, msg.as_string())


def main(filename):
    attachment = make_attachment(open(filename, "rb").read(),
                                 os.path.basename(filename))
    send("pacholick@gmail.com",
         subject="Předmět",
         body="Tělo",
         attachments=[attachment])


if __name__ == "__main__":
    main(sys.argv[1:])
