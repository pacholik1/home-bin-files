#!/usr/bin/python3

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2018 Pachol, Vojtěch <pacholick@gmail.com>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

"""Create supermusic songbook for kindle"""
# deps: mkc, beautifulsoup4

import os
from datetime import datetime
from bs4 import BeautifulSoup

import mkc


def make_multiple_page(articles, name="supermusic", titles=False):
    filename = os.path.join(mkc.CACHE_DIR, name + ".html")
    with open(filename, "w") as f:
        f.write(
            """<!DOCTYPE HTML PUBLIC
            "-//W3C//DTD HTML 4.0//EN"
            "http://www.w3.org/TR/1998/REC-html40-19980424/strict.dtd">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>""")
        f.write(name)
        f.write("""</title>
                </head>
                <body>
                <font size=1>
                """)

        # TOC
        for i in articles:
            f.write("""<a href="#{title}">{title}<br/></a>
                    """.format(title=i.title))

        # content
        for i in articles:
            f.write("""
                    <a style="page-break-before:always" name="{title}"></a>
                    """.format(title=i.title))
            f.write(i.content)

        f.write("</body></html>")

    return filename


def main(*argv):
    with open("/home/pacholik/scores/supermusic.html") as f:
        soup = BeautifulSoup(f, 'lxml')
    articles = [
        mkc.get_article(
            "http://www.supermusic.cz/piesen_tlac.php{}".format(e.get('href')),
            imgs=False, maths=False)
        for e in soup.findAll('a')
    ]

    page = make_multiple_page(
        articles, name=datetime.now().strftime('supermusic_%Y-%m-%d'))
    print(mkc.html_to_mobi(page))


if __name__ == '__main__':
    main()
